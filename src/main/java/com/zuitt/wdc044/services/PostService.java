package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    //Create a post method
    void createPost(String stringToken, Post post);

    //Getting all posts
    Iterable<Post> getPosts();

    //Edit a user's post method
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //Delete a user's post method
    ResponseEntity deletePost(Long id, String stringToken);

}

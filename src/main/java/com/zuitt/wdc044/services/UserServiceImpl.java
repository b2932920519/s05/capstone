package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.zuitt.wdc044.repositories.PostRepository;

import java.util.List;
import java.util.Optional;
@Service
public class UserServiceImpl implements UserService{

   @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    PostRepository postRepository;

   public void createUser(User user){
       //Saves the user in our User Table
       userRepository.save(user);
   }

   public Optional<User> findByUsername(String username){
       return Optional.ofNullable(userRepository.findByUsername(username));
   }

   @Override
   public User getUser(String stringToken){
       String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        return userRepository.findByUsername(authenticatedUser);
   }

    @Override
    public Iterable<Post> myPosts(String stringToken) {
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User foundUser = userRepository.findByUsername(authenticatedUser);
        Long id = foundUser.getId();

        List<Post> posts = postRepository.findByUserId(id);
        return posts;
    }

}
